<?php
require '/var/www/api/public/vendor/autoload.php';

use \GuzzleHttp\Client;
use \GuzzleHttp\Handler\MockHandler;
use \GuzzleHttp\HandlerStack;
use \GuzzleHttp\Psr7\Response;
use \GuzzleHttp\Psr7\Request;
use \GuzzleHttp\Exception\RequestException;

/**
* Class ProductTest.
*/
class ProductTest extends \PHPUnit\Framework\TestCase {

    protected $request;

    public function setUp() {
        $this->request = new GuzzleHttp\Client([
            'base_uri' => 'http://nginx',
            'headers' => []
        ]);
    }

    public function test_case_get_all_product(){
        $response = $this->request->get('/product', []);
        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getBody(), true);
    }

    public function test_case_get_one_product_invalid(){
        $response = $this->request->post(
            '/category/create', 
            [GuzzleHttp\RequestOptions::MULTIPART, [
                'name' => 'Name', 
                'code' => 'Code',
                'price' => 10,
                'quantity' => 10,
                'description' => 'Description test',
            ]]
        );
        $this->assertEquals(200, $response->getStatusCode());
    }
}
