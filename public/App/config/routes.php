<?php
use Core\Router;
use Core\LoadController;

$router = new Router;
$router
->on('GET', '/', function() {
  new LoadController("dashboard\IndexController");
})
->on('GET', '/category', function() {
  new LoadController("category\IndexController");
})
->on('GET', '/category/new', function() {
  new LoadController("category\NewController");
})
->on('GET', '/category/edit/(.*)', function ($id)  {
  new LoadController("category\EditController", $id);
})
->on('GET', '/category/delete/(.*)', function ($id)  {
  new LoadController("category\DeleteController", $id);
})
->on('POST', '/category/update/(.*)', function ($id)  {
  new LoadController("category\UpdateController", $id);
})
->on('POST', '/category/create', function ()  {
  new LoadController("category\CreateController");
})
->on('GET', '/product', function ()  {
  new LoadController("product\IndexController");
})
->on('GET', '/product/new', function ()  {
  new LoadController("product\NewController");
})
->on('GET', '/product/edit/(.*)', function ($id)  {
  new LoadController("product\EditController", $id);
})
->on('GET', '/product/delete/(.*)', function ($id)  {
  new LoadController("product\DeleteController", $id);
})
->on('POST', '/product/update/(.*)', function ($id)  {
  new LoadController("product\UpdateController", $id);
})
->on('POST', '/product/create', function ()  {
  new LoadController("product\CreateController");
});
echo $router($router->method(), $router->uri());