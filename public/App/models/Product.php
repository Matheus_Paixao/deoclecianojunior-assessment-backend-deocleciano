<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
* Controller Base, Contains the low level controller rule.
*
*
* @package   WebJump Challenge
* @category  Core System
* @author    Deocleciano Júnior
*/
class Product extends Model 
{

    /*
    * Set default table name 
    *
    * @var string
    */

    protected $table = 'product';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */

    protected $fillable = ['name', 'code', 'price', 'description', 'quantity'];

    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */

    protected $hidden = ['created_at', 'updated_at'];

    public function productCategory()
    {
        return $this->hasMany(\App\Models\ProductCategory::class);
    }

}