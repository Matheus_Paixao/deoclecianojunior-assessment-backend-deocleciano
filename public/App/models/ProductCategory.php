<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
* Controller Base, Contains the low level controller rule.
*
*
* @package   WebJump Challenge
* @category  Core System
* @author    Deocleciano Júnior
*/
class ProductCategory extends Model 
{
    /*
    * Set default table name 
    *
    * @var string
    */

    protected $table = 'product_category';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */

    protected $fillable = ['category_id', 'product_id',];

    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */

    protected $hidden = ['created_at', 'updated_at'];

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
}