<?php
namespace App\Controllers\Product;

use Core\BaseController;
use App\Models\Product;
/**
* Controller Base, Contains the low level controller rule.
*
*
* @package   WebJump Challenge
* @category  Core System
* @author    Deocleciano Júnior
*/
class IndexController extends BaseController
{

    function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
        $products = Product::all();
        $this->view->render('product/index.html', ['products' => $products]);
    }
}
