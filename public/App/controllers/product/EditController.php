<?php
namespace App\Controllers\Product;

use Core\BaseController;
use App\Models\Product;
use App\Models\Category;
use App\Models\ProductCategory;
/**
* Controller Base, Contains the low level controller rule.
*
*
* @package   WebJump Challenge
* @category  Core System
* @author    Deocleciano Júnior
*/
class EditController extends BaseController
{

    private $id;

    function __construct($id)
    {
        parent::__construct();
        $this->id = $id;
    }

    public function run()
    {
        $product = Product::find($this->id);
        $categories = Category::all();
        $arrayProductCategoryIds = $this->getIdProductCategory($product->productCategory);
        $this->view->render(
            'product/edit.html',
            [
                'product' => $product,
                'categories' => $categories,
                'arrayProductCategoryIds' => $arrayProductCategoryIds
            ]
        );
    }

    private function getIdProductCategory($productCategory){
        $arrayProductCategoryIds = [];
        foreach ($productCategory as $key => $pc) {      
            $arrayProductCategoryIds[] = $pc->category_id;
        }
        return $arrayProductCategoryIds;
    }
}
