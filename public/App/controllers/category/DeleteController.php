<?php
namespace App\Controllers\Category;

use Core\BaseController;
use App\Models\Category;
/**
* Controller Base, Contains the low level controller rule.
*
*
* @package   WebJump Challenge
* @category  Core System
* @author    Deocleciano Júnior
*/
class DeleteController extends BaseController
{

    private $id;

    function __construct($id)
    {
        parent::__construct();
        $this->id = $id;
    }

    public function run()
    {
        $category = Category::find($this->id);
        $category->delete();
        $this->redirect('/category');
    }
}
