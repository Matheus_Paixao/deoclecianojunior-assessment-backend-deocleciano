<?php
namespace Core;

use \Twig\Loader\FilesystemLoader;
use \Twig\Environment;

class View 
{  
  protected $twig;
  private $path_twig = BASE_PATH.'App/views';
  private $path_cache_twig = BASE_PATH.'App/views/cache';
  private $variables_default = [];
  

  public function __construct()
  {
    $loader = new FilesystemLoader($this->path_twig);
    $this->twig = new Environment($loader, [
        'debug' => true,
        'cache' => false
    ]);
    $this->setDEfaultVariables();
  }

  public function render($view, $data = [])
  {
    $data = $this->addDefaultVariablesToView($data);
    print $this->twig->render($view, $data);
  }

  private function addDefaultVariablesToView($data){
    foreach ($this->variables_default as $key => $variable) {
      $data[$key] = $variable;
    }
    return $data;
  }

  private function setDefaultVariables(){
    $this->variables_default['base_url'] = BASE_URL;
    $this->variables_default['base_path'] = BASE_PATH;
  }
}
