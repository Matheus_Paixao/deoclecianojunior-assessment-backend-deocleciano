<?php
namespace Core;

use Core\Output;
use Core\Input;
use Core\View;
use Core\FileUploader;

/**
 * Controller Base, Contains the low level controller rule.
 *
 *
 * @package   WebJump Challenge
 * @category  Core System
 * @author    Deocleciano Júnior
 */
class BaseController 
{

  protected $input;
  protected $view;
  protected $fileUploader;
  
  public function __construct() 
  {
    $this->input = new Input;
    $this->view = new View;
    $this->fileUploader = new FileUploader;
  }

  protected function redirect($path){
    header("Location:".$path);
  }
}