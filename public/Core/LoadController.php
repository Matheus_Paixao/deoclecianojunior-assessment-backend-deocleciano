<?php
namespace Core;

use Core\Interfaces\ILoader;

class LoadController
{  
  private $className;
  private $args;
  
  public function __construct($className, $args = false){
    $this->className = $className;
    $this->args = $args;
    $this->load();
  }

  private function load()
  {
    $class = new \ReflectionClass("\\App\\Controllers\\".$this->className);
    $instance = $class->newInstance($this->args);
    $instance->run();
  }
}
